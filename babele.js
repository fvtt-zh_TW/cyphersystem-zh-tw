Hooks.once('init', () => {
    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'cyphersystem-zh-tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });        
    }
});

